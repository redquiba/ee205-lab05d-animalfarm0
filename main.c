/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalfarm 0 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// @author Ryan Edquiba <redquiba@hawaii.edu>
/// @date 17_Feb_2022
/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
